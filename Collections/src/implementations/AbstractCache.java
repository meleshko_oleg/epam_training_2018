package implementations;

import interfaces.Cache;
import interfaces.SizeCalculator;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Avoid copy-paste
 * @param <T> Cache object type.
 * @param <Q> Queue type.
 */
abstract class AbstractCache<T, Q extends Comparable<Q>> implements Cache<T> {
    private int cacheCapacity;
    Map<T, Integer> innerCache;             // holds an object and its sizeCalculator (depends on cache capacity)
    Map<T,Q> queue;                         // queue realisation
    SizeCalculator<T> sizeCalculator;       // it can calculate object size
    Lock lock;                              // multithreading protection

    AbstractCache(int cacheCapacity) {
        this.cacheCapacity = cacheCapacity;
        lock = new ReentrantLock();
        innerCache = new HashMap<>();
        queue = new HashMap<>();

        /* it's a realisation for objects that implement a Serializable interface :) */
        /* calculate the number of bytes of which an object consists (i hope :) )*/
        sizeCalculator = object -> {
            ByteArrayOutputStream byteArrayOutputStream = null;
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(object);
                objectOutputStream.flush();
                objectOutputStream.close();
                byteArrayOutputStream.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return byteArrayOutputStream.toByteArray().length;
        };
    }

    /**
     * Check cache size and remove unnecessary value if cache is full.
     */
    void checkCacheSize(){
        if (innerCache.size() == cacheCapacity) {
            queue.entrySet().stream()
                    .min(Map.Entry
                    .comparingByValue())
                    .ifPresent(entry -> {
                        final T elem = entry.getKey();
                        innerCache.remove(elem);
                        queue.remove(elem);
                    });
        }
    }

    /**
     * Get cache info.
     */
    @Override
    public String getCacheInfo() {
        return "Inner cache:\n" + innerCache + "\n" + getQueue();
    }

    /**
     * Get info stored in queues (Realisation in subclasses).
     */
    abstract String getQueue();
}