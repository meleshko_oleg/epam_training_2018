package implementations;

/**
 * Cache with LFU eviction algorithm.
 * @param <T> cache object type.
 */
public class LFUCache<T> extends AbstractCache<T, Integer> {

    public LFUCache(int cacheCapacity) {
        super(cacheCapacity);
    }

    /**
     * Get object sizeCalculator from LFU cache.
     * @param object whose sizeCalculator we search.
     * @return byte count.
     */
    @Override
    public int getObjectSizeFromCacheOrCalculate(T object) {
        final int FIRST_APPEAL = 1;
        try {
            lock.lock();

            /* return from cache */
            if (innerCache.containsKey(object)) {
                int frequency = queue.get(object);
                queue.put(object, ++frequency);
                return innerCache.get(object);
            }

            /* invoke a calculate method */
            else {
                int objectSize = sizeCalculator.calculateObjectSize(object);

                /* check cache size and remove unnecessary value if cache is full */
                checkCacheSize();

                innerCache.put(object, objectSize);
                queue.put(object, FIRST_APPEAL);
                return objectSize;
            }
        }
        finally {
            lock.unlock();
        }
    }

    /**
     * Get string representation of the frequency queue.
     * @return info stored in frequency queue.
     */
    @Override
    String getQueue() {
        return "Frequency queue:\n" + queue + "\n";
    }
}