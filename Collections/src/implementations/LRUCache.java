package implementations;

import java.util.Date;

/**
 * Cache with LRU eviction algorithm.
 * @param <T> cache object type.
 */
public class LRUCache <T> extends AbstractCache<T, Date> {

    public LRUCache(int cacheCapacity) {
        super(cacheCapacity);
    }

    /**
     * Get object sizeCalculator from LRU cache.
     * @param object whose sizeCalculator we search.
     * @return byte count.
     */
    @Override
    public int getObjectSizeFromCacheOrCalculate(T object) {
        final Date CURRENT_DATE = new Date();                      // current time of calculating
        try {
            lock.lock();

            /* return from cache */
            if (innerCache.containsKey(object)) {
                queue.put(object, CURRENT_DATE);
                return innerCache.get(object);
            }

            /* invoke a calculate method */
            else {
                int objectSize = sizeCalculator.calculateObjectSize(object);

                /* check cache size and remove unnecessary value if cache is full */
                checkCacheSize();

                innerCache.put(object, objectSize);
                queue.put(object, CURRENT_DATE);
                return objectSize;
            }
        }
        finally {
            lock.unlock();
        }
    }

    /**
     * Get string representation of the time queue.
     * @return info stored in time queue.
     */
    @Override
    String getQueue() {
        return "Time queue:\n" + queue + "\n";
    }
}