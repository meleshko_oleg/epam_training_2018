package customClasses;

import java.io.Serializable;
import java.util.Objects;

/**
 * Simple class A.
 */
public class A implements Serializable {
    private String someString;

    public A(String someString) {
        this.someString = someString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        A a = (A) o;
        return Objects.equals(someString, a.someString);
    }

    @Override
    public int hashCode() {

        return Objects.hash(someString);
    }

    @Override
    public String toString() {
        return String.format("Class A object: someString = %s ", someString);
    }
}