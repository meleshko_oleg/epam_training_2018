package customClasses;

import java.io.Serializable;
import java.util.Objects;

/**
 * Simple class B.
 */
public class B implements Serializable {
    private int someInt;
    private String someString;

    public B(int someInt, String someString) {
        this.someInt = someInt;
        this.someString = someString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        B b = (B) o;
        return someInt == b.someInt &&
                Objects.equals(someString, b.someString);
    }

    @Override
    public int hashCode() {

        return Objects.hash(someInt, someString);
    }

    @Override
    public String toString() {
        return String.format("Class B object: someInt = %d, someString = %s ", someInt, someString);
    }
}