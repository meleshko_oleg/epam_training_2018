import customClasses.A;
import customClasses.B;
import implementations.LFUCache;
import implementations.LRUCache;
import interfaces.Cache;
import printers.CachePrinter;

/**
 * Runner class.
 */
public class Main {

    public static void main(String[] args) {

        Cache<A> cacheA = new LFUCache<>(5);
        Cache<B> cacheB = new LRUCache<>(7);

        //Cache<String> stringCache = new LFUCache<>(10);
        //Cache<Integer> integerCache = new LRUCache<>(9);
        //Cache<Exception> exceptionCache = new LFUCache<>(15);

        /* First thread for working with LFUCache */
        Thread cacheAFirstThread = new Thread(() -> {
            cacheA.getObjectSizeFromCacheOrCalculate(new A("a"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaaa"));
        });

        /* Second thread for working with LFUCache */
        Thread cacheASecondThread = new Thread(() -> {
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("a"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("a"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaa"));
        });

        /* Third thread for working with LFUCache */
        Thread cacheAThirdThread = new Thread(() -> {
            cacheA.getObjectSizeFromCacheOrCalculate(new A("a"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaaaaaaaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaa"));
            cacheA.getObjectSizeFromCacheOrCalculate(new A("aaaa"));
        });

        /* First thread for working with LRUCache */
        Thread cacheBFirstThread = new Thread(() -> {
            cacheB.getObjectSizeFromCacheOrCalculate(new B(1, "b"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(2, "bb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(3, "bbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(4, "bbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(5, "bbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(6, "bbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(7, "bbbbbbb"));
        });

        /* Second thread for working with LRUCache */
        Thread cacheBSecondThread = new Thread(() -> {
            cacheB.getObjectSizeFromCacheOrCalculate(new B(1, "b"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(1, "b"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(1, "b"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(3, "bbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(8, "bbbbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(9, "bbbbbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(3, "bbb"));
        });

        /* Third thread for working with LRUCache */
        Thread cacheBThirdThread = new Thread(() -> {
            cacheB.getObjectSizeFromCacheOrCalculate(new B(10, "bbbbbbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(1, "b"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(5, "bbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(7, "bbbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(11, "bbbbbbbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(9, "bbbbbbbbb"));
            cacheB.getObjectSizeFromCacheOrCalculate(new B(3, "bbb"));
        });

        /* Start all thread for LFUCache and LRUCache */
        cacheAFirstThread.start();
        cacheASecondThread.start();
        cacheAThirdThread.start();

        cacheBFirstThread.start();
        cacheBSecondThread.start();
        cacheBThirdThread.start();

        /* Current thread will be waiting for the end of all cache threads */
        try {
            cacheAFirstThread.join();
            cacheASecondThread.join();
            cacheAThirdThread.join();

            cacheBFirstThread.join();
            cacheBSecondThread.join();
            cacheBThirdThread.join();
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }

        /* Print all caches */
        CachePrinter.printCacheInfo(cacheA);
        CachePrinter.printCacheInfo(cacheB);
    }
}