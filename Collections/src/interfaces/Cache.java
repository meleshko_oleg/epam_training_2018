package interfaces;

/**
 * Cache with object sizes.
 * @param <T>
 */
public interface Cache <T>{

    /**
     * Get object size from cache or use a SizeCalculator.
     * @param object whose size we search.
     * @return int representation of object size.
     */
    int getObjectSizeFromCacheOrCalculate(T object);

    /**
     * Test method.
     */
    String getCacheInfo();
}