package interfaces;

/**
 * Calculate object size.
 * @param <T> object type.
 */
@FunctionalInterface
public interface SizeCalculator<T> {

    /**
     * Calculate object size.
     * @param object object whose size will be calculated.
     * @return int representation of object size
     */
    int calculateObjectSize(T object);
}