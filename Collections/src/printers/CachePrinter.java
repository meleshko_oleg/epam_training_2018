package printers;

import interfaces.Cache;

/**
 * Print cache info.
 */
public class CachePrinter {

    /**
     * Print cache info to the console.
     * @param cache Cache interface implementation.
     */
    public static void printCacheInfo(Cache cache){
        System.out.println(cache.getCacheInfo());
    }
}