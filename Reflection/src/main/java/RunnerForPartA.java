import partA.factories.InstanceFactory;
import partA.interfaces.Printer;
import org.reflections.Reflections;
import java.util.Set;

/**
 * Runner class for part A of the Reflection Home Task.
 */
public class RunnerForPartA {
    
    public static void main(String[] args) {
        final String PACKAGE_NAME_WITH_CLASSES_FOR_TESTING = "partA.implementations";

        // use additional library
        Reflections reflections = new Reflections(PACKAGE_NAME_WITH_CLASSES_FOR_TESTING);

        // all classes that implement Printer interface
        Set<Class<? extends Printer>> printers = reflections.getSubTypesOf(Printer.class);

        // get an instance of each class from the Set and invoke the method print()
        for (Class printerClass : printers) {
            try {
                Printer printer = InstanceFactory.getInstanceOf(printerClass);
                printer.print();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}