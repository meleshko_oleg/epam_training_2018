import partB.analyzers.CarAnalyzer;
import partB.enums.CarModel;
import partB.enums.Color;
import partB.objects.Car;

/**
 * Runner class for part B of the Reflection Home Task.
 */
public class RunnerForPartB {

    public static void main(String[] args) {

        // N1
        Car bmw5 = new Car(CarModel.BMW, 1550, 5, Color.BLACK, "Germany");
        Car _bmw5 = new Car(CarModel.BMW, 1550, 5, Color.BLACK, "Germany");
        boolean areCarsEquivalent_1 = CarAnalyzer.areCarsEquivalent(bmw5, _bmw5);               // true
        printResultOfCarComparison(bmw5, _bmw5, areCarsEquivalent_1);

        // N2
        Car bmw3 = new Car(CarModel.BMW, 1110, 4, Color.BLUE, "Germany");
        Car _bmw3 = new Car(CarModel.BMW, 1110, 4, Color.BLUE, new String("Germany"));
        boolean areCarsEquivalent_2 = CarAnalyzer.areCarsEquivalent(bmw3, _bmw3);               // false (last constructor parameter without interning)
        printResultOfCarComparison(bmw3, _bmw3, areCarsEquivalent_2);

        // N3
        Car audiA8 = new Car(CarModel.AUDI, 1950, 5, Color.WHITE, "Brazil");
        Car _audiA8 = new Car(CarModel.AUDI, 1950, 5, Color.GREY, "Brazil");
        boolean areCarsEquivalent_3 = CarAnalyzer.areCarsEquivalent(audiA8, _audiA8);           // false (colors differ)
        printResultOfCarComparison(audiA8, _audiA8, areCarsEquivalent_3);

        // N4
        Car mazda3 = new Car(CarModel.MAZDA, 1150, 4, Color.BLUE, "Japan");
        Car mazda6 = new Car(CarModel.MAZDA, 1750, 5, Color.GREY, "Japan");
        boolean areCarsEquivalent_4 = CarAnalyzer.areCarsEquivalent(mazda3, mazda6);            // false (weight, passenger capacity and color differ)
        printResultOfCarComparison(mazda3, mazda6, areCarsEquivalent_4);

        // N5
        Car ferrariF430 = new Car(CarModel.FERRARI, 1450, 2, Color.RED, "Italy");
        Car _ferrariF430 = new Car(CarModel.FERRARI, 1450, 2, Color.RED, "Italy");
        boolean areCarsEquivalent_5 = CarAnalyzer.areCarsEquivalent(ferrariF430, _ferrariF430); // true
        printResultOfCarComparison(ferrariF430, _ferrariF430, areCarsEquivalent_5);

        // N6
        Car ladaVesta = new Car(CarModel.LADA, 1200, 4, Color.YELLOW, "Russia");
        Car _ladaVesta = new Car(CarModel.LADA, 1200, 4, Color.YELLOW, "Kazakhstan");
        boolean areCarsEquivalent_6 = CarAnalyzer.areCarsEquivalent(ladaVesta, _ladaVesta);      // false (manufacturer countries differ)
        printResultOfCarComparison(ladaVesta, _ladaVesta, areCarsEquivalent_6);

        // N7
        Car mercedes = new Car(CarModel.MERCEDES, 2200, 5, Color.GREEN, "China");
        Car lexus = new Car(CarModel.LEXUS, 2200, 5, Color.GREEN, "China");
        boolean areCarsEquivalent_7 = CarAnalyzer.areCarsEquivalent(mercedes, lexus);            // false (car models differ)
        printResultOfCarComparison(mercedes, lexus, areCarsEquivalent_7);
    }

    /**
     * Print result of comparison to console.
     * @param firstCar for comparison.
     * @param secondCar for comparison.
     * @param areCarsEquivalent result of comparison.
     */
    private static void printResultOfCarComparison(Car firstCar, Car secondCar, boolean areCarsEquivalent){
        System.out.printf(
                "First car - %s\n" +
                "Second car - %s\n" +
                "Are these cars equivalent? ---> %b\n\n",
                firstCar, secondCar, areCarsEquivalent);
    }
}