package partA.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Handler for proxy.
 */
public class PrinterInvocationHandler implements InvocationHandler{

    /**
     * Method substitution.
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final String RESULT_STRING = "I'm not a printer. I'm a proxy!!! I have overridden a method with name: %s\n";
        System.out.printf(RESULT_STRING, method.getName());
        return new Object[0];
    }
}