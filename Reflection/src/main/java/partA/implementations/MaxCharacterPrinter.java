package partA.implementations;

import partA.interfaces.Printer;

/**
 * Get info about the maximum value of character.
 */
public class MaxCharacterPrinter implements Printer {

    /**
     * Print to console the maximum value of character.
     */
    @Override
    public void print(){
        System.out.printf("The maximum value of character: %d\n", (int)Character.MAX_VALUE);
    }
}