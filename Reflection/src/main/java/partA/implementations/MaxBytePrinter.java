package partA.implementations;

import partA.annotations.Proxy;
import partA.interfaces.Printer;

/**
 * Get info about the maximum value of byte.
 */
@Proxy(invocationHandler = "partA.handlers.PrinterInvocationHandler")
public class MaxBytePrinter implements Printer {

    /**
     * Print to console the maximum value of byte.
     */
    @Override
    public void print() {
        System.out.printf("The maximum value of byte: %d\n", Byte.MAX_VALUE);
    }
}