package partA.implementations;

import partA.interfaces.Printer;

/**
 * Get info about the maximum value of short.
 */
public class MaxShortPrinter implements Printer {

    /**
     * Print to console the maximum value of short.
     */
    @Override
    public void print() {
        System.out.printf("The maximum value of short: %d\n", Short.MAX_VALUE);
    }
}