package partA.implementations;

import partA.interfaces.Printer;

/**
 * Get info about the maximum value of long.
 */
public class MaxLongPrinter implements Printer {

    /**
     * Print to console the maximum value of long.
     */
    @Override
    public void print() {
        System.out.printf("The maximum value of long: %d\n", Long.MAX_VALUE);
    }
}