package partA.implementations;

import partA.interfaces.Printer;

/**
 * Get info about the maximum value of float.
 */
public class MaxFloatPrinter implements Printer {

    /**
     * Print to console the maximum value of float.
     */
    @Override
    public void print() {
        System.out.printf("The maximum value of float: %e\n", Float.MAX_VALUE);
    }
}