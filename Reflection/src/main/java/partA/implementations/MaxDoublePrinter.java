package partA.implementations;

import partA.annotations.Proxy;
import partA.interfaces.Printer;

/**
 * Get info about the maximum value of double.
 */
@Proxy(invocationHandler = "partA.handlers.PrinterInvocationHandler")
public class MaxDoublePrinter implements Printer {

    /**
     * Print to console the maximum value of double.
     */
    @Override
    public void print() {
        System.out.printf("The maximum value of double: %e\n", Double.MAX_VALUE);
    }
}