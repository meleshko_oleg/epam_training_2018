package partA.implementations;

import partA.annotations.Proxy;
import partA.interfaces.Printer;

/**
 * Get info about the maximum value of integer.
 */
@Proxy(invocationHandler = "partA.handlers.PrinterInvocationHandler")
public class MaxIntegerPrinter implements Printer{

    /**
     * Print to console the maximum value of integer.
     */
    @Override
    public void print() {
        System.out.printf("The maximum value of integer: %d\n", Integer.MAX_VALUE);
    }
}