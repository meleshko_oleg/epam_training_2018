package partA.interfaces;

/**
 * interface used for proxy.
 */
public interface Printer {

    /**
     * Print some information
     */
    void print();
}