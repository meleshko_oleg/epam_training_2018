package partA.factories;

import partA.annotations.Proxy;
import partA.interfaces.Printer;
import java.lang.reflect.InvocationHandler;

/**
 * Factory to get an instance of a class.
 */
public class InstanceFactory {

    /**
     * Factory method to get an instance of a class.
     * @param clazz a Class name.
     * @return an implementation of Printer interface or a proxy.
     */
    public static Printer getInstanceOf(Class clazz) {
        Printer printer = null;
        Proxy proxyAnnotation = (Proxy)clazz.getAnnotation(Proxy.class);

        if (proxyAnnotation != null){
            printer = createProxy(clazz);                       // class is annotated with @Proxy => get a proxy
        }
        else {
            try {
                printer = (Printer)clazz.newInstance();         // class isn't annotated with @Proxy => get an implementation of the interface
            }
            catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return printer;
    }

    /**
     * Additional method to create a proxy.
     * @param clazz Class with @Proxy annotation.
     * @return a proxy for this class.
     */
    private static Printer createProxy(Class clazz){
        InvocationHandler invocationHandler = null;
        Proxy proxyAnnotation = (Proxy)clazz.getAnnotation(Proxy.class);
        String invocationHandlerClassName = proxyAnnotation.invocationHandler();        // name of the invocation handler class

        try {
            Class invocationHandlerClass = Class.forName(invocationHandlerClassName);
            invocationHandler = (InvocationHandler) invocationHandlerClass.newInstance();
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        // get a proxy for Class
        Printer proxy = (Printer)java.lang.reflect.Proxy.newProxyInstance(
                                                                        Printer.class.getClassLoader(),
                                                                        new Class[] { Printer.class },
                                                                        invocationHandler);

        return proxy;
    }
}