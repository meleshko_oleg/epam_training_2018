package partB.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for objects equality.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Equal {
    /**
     * reference - to determine objects equality by reference.
     * value - to determine objects equality by value (state).
     */
    String compareBy();     // "reference" or "value"
}