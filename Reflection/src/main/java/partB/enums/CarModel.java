package partB.enums;

/**
 * Car models.
 */
public enum CarModel {
    BMW, MAZDA, AUDI, LEXUS, MERCEDES, LADA, FERRARI
}