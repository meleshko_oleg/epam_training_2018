package partB.enums;

/**
 * Car colors.
 */
public enum Color {
    WHITE, BLACK, RED, BLUE, YELLOW, GREY, GREEN
}