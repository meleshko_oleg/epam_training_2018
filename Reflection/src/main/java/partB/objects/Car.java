package partB.objects;

import partB.annotations.Equal;
import partB.enums.CarModel;
import partB.enums.Color;

/**
 * Car.
 */
public class Car {
    @Equal(compareBy = "value")
    private CarModel carModel;

    @Equal(compareBy = "value")
    private int weight;

    @Equal(compareBy = "reference")         // Integer pool (-128...127) will help us
    private int passengerCapacity;

    @Equal(compareBy = "reference")
    private Color color;

    @Equal(compareBy = "reference")
    private String manufacturerCountry;     // depends on String.intern(): "some string" or new String("some string")

    public Car(CarModel carModel, int weight, int passengerCapacity, Color color, String manufacturerCountry) {
        this.carModel = carModel;
        this.weight = weight;
        this.passengerCapacity = passengerCapacity;
        this.color = color;
        this.manufacturerCountry = manufacturerCountry;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public int getWeight() {
        return weight;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public Color getColor() {
        return color;
    }

    public String getManufacturerCountry() {
        return manufacturerCountry;
    }

    /**
     * Complex getter.
     * @param fieldName 1 of 5 fields.
     * @return a simple getter.
     */
    public Object getFieldValueByName(String fieldName){
        switch (fieldName){
            case "carModel":
                return getCarModel();
            case "weight":
                return getWeight();
            case "passengerCapacity":
                return getPassengerCapacity();
            case "color":
                return getColor();
            case "manufacturerCountry":
                return getManufacturerCountry();
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        return String.format("car model: %s, weight: %d kg, passenger capacity: %d, color: %s, manufacturer country: %s",
                            carModel, weight, passengerCapacity, color, manufacturerCountry);
    }
}