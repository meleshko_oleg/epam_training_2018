package partB.analyzers;

import partB.annotations.Equal;
import partB.objects.Car;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Car instances analyzer.
 */
public class CarAnalyzer {

    /**
     * Compare two cars.
     * @param firstCar a first car for comparison.
     * @param secondCar a second car for comparison.
     * @return result of comparison.
     */
    public static boolean areCarsEquivalent(Object firstCar, Object secondCar){
        Map<Field, String> fieldComparison = new HashMap<>();                   // how compare two fields (by reference or by value)

        Field[] fields = firstCar.getClass().getDeclaredFields();
        for (Field field: fields){
            Equal equalAnnotation = field.getAnnotation(Equal.class);
            if (equalAnnotation != null){
                fieldComparison.put(field, equalAnnotation.compareBy());
            }
        }

        return areCarsEquivalent(fieldComparison, firstCar, secondCar);         // invoke an additional comparison method
    }

    /**
     * Additional method of comparing two cars.
     * @param fieldComparison a map with info about field comparison (by reference or by value).
     * @param firstCar a first car for comparison.
     * @param secondCar a second car for comparison.
     * @return result of comparison.
     */
    private static boolean areCarsEquivalent(Map<Field, String> fieldComparison, Object firstCar, Object secondCar){
        boolean  areCarsEquivalent = true;
        String COMPARISON_BY_REFERENCE = "reference";
        String COMPARISON_BY_VALUE = "value";

        Car _firstCar = (Car) firstCar;
        Car _secondCar = (Car) secondCar;

        Set<Map.Entry<Field, String>> entries = fieldComparison.entrySet();
        for (Map.Entry<Field, String> entry : entries) {
            String compareBy = entry.getValue();                // by reference or by value
            String fieldName = entry.getKey().getName();        // carModel / weight / passengerCapacity / color / manufacturerCountry

            if (compareBy.equals(COMPARISON_BY_REFERENCE)) {    // if by reference
                if (_firstCar.getFieldValueByName(fieldName) != _secondCar.getFieldValueByName(fieldName)) {
                    areCarsEquivalent = false;
                }
            }
            else if (compareBy.equals(COMPARISON_BY_VALUE)) {   // if by value
                if (!_firstCar.getFieldValueByName(fieldName).equals(_secondCar.getFieldValueByName(fieldName))) {
                    areCarsEquivalent = false;
                }
            }
        }

        return areCarsEquivalent;
    }
}