package main.java.validation.checkers;

import main.java.validation.Checker;
import org.apache.tools.ant.Project;

/**
 * Abstract class to avoid copy-paste
 */
abstract class AbstractChecker implements Checker {
    Project project;

    AbstractChecker(Project project) {
        this.project = project;
    }

    /**
     * Realisation in subclasses
     */
    @Override
    public boolean checkFile() {
        return false;
    }
}