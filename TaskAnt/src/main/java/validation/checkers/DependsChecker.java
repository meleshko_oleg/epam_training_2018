package main.java.validation.checkers;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;
import java.util.Hashtable;
import java.util.Vector;

/**
 * If targets with depends are used instead of "main" point
 */
public class DependsChecker extends AbstractChecker{
    public DependsChecker(Project project) {
        super(project);
    }

    @Override
    public boolean checkFile() {
        final String DEFAULT_TARGET = "main";
        String defaultTarget = project.getDefaultTarget();

        if (defaultTarget == null){                                         // avoid a problem when we check a project without a default target
            defaultTarget = DEFAULT_TARGET;
        }

        Hashtable<String, Target> tableOfAllTargets = project.getTargets();                                 // all targets + 1 empty element (mystic ^^)
        Vector<Target> targets = project.topoSort(defaultTarget, tableOfAllTargets);
        Vector<Target> mainDependedTargets = project.topoSort(defaultTarget, tableOfAllTargets, false);     // targets, that depend on main target
        targets.removeAll(mainDependedTargets);                             // targets, that don't depend on main target
        return targets.size() > 1;                                          // i don't now why, but i have 1 empty element when the list must be empty :)
    }
}