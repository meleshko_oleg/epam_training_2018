package main.java.validation.checkers;

import org.apache.tools.ant.Project;

/**
 * If project contains default attribute
 */
public class DefaultsChecker extends AbstractChecker {
    public DefaultsChecker(Project project) {
        super(project);
    }

    @Override
    public boolean checkFile() {
        String defaultTarget = project.getDefaultTarget();
        return defaultTarget != null;
    }
}