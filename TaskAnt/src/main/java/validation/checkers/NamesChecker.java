package main.java.validation.checkers;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;
import java.util.Hashtable;
import java.util.Vector;

/**
 * If names contains only letters with '-'
 */
public class NamesChecker extends AbstractChecker {
    public NamesChecker(Project project) {
        super(project);
    }

    @Override
    public boolean checkFile() {
        final String DEFAULT_TARGET = "main";
        final String PATTERN = "[A-Za-z]+-[A-Za-z]+";                               // only letters with '-'
        boolean containsLetterWithDash = true;

        Hashtable<String, Target> tableOfAllTarget = project.getTargets();
        String defaultTarget = project.getDefaultTarget();
        if (defaultTarget == null){                                                 // avoid a problem when we check a project without a default target
            defaultTarget = DEFAULT_TARGET;
        }

        Vector<Target> targets = project.topoSort(defaultTarget, tableOfAllTarget); // get all targets
        for (Target target: targets){
            if (!target.getName().isEmpty() && !target.getName().matches(PATTERN)){
                containsLetterWithDash = false;
            }
        }
        return containsLetterWithDash;
    }
}