package main.java.validation;

import main.java.validation.checkers.DefaultsChecker;
import main.java.validation.checkers.DependsChecker;
import main.java.validation.checkers.NamesChecker;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Task;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Validator for xml file
 */
public class Validator extends Task {
    private boolean checkDepends;
    private boolean checkDefaults;
    private boolean checkNames;
    private List<BuildFile> buildFiles = new ArrayList<>();     //sub elements

    public Validator() {
    }

    /**
     * Create a sub element of validator
     * @return new instance of sub element
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    public void setCheckDepends(boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    public void setCheckDefaults(boolean checkDefaults) {
        this.checkDefaults = checkDefaults;
    }

    public void setCheckNames(boolean checkNames) {
        this.checkNames = checkNames;
    }

    public boolean isCheckDepends() {
        return checkDepends;
    }

    public boolean isCheckDefaults() {
        return checkDefaults;
    }

    public boolean isCheckNames() {
        return checkNames;
    }

    @Override
    public void execute() {
        final String DEPENDS_RESULT = "If targets with depends are used instead of 'main' point: ";
        final String DEFAULT_RESULT = "If project contains default attribute: ";
        final String NAMES_RESULT = "If names contains only letters with '-': ";
        for (BuildFile buildfile: buildFiles) {
            Project project = new Project();
            ProjectHelper.configureProject(project, new File(buildfile.getLocation()));     // find a file by name
            if (checkDepends) {
                log(DEPENDS_RESULT + new DependsChecker(project).checkFile());              // log results of depends checking to console using ant logger
            }
            if (checkDefaults) {
                log(DEFAULT_RESULT + new DefaultsChecker(project).checkFile());             // log results of defaults checking to console using ant logger
            }
            if (checkNames) {
                log(NAMES_RESULT + new NamesChecker(project).checkFile());                  // log results of names checking to console using ant logger
            }
        }
    }

    /**
     * Class that describe a sub element of validator
     */
    public class BuildFile {
        private String location;

        public BuildFile() {
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }
    }
}