package main.java.validation;

/**
 * Main interface
 */
public interface Checker {
    /**
     *
     * @return result of file checking
     */
    boolean checkFile();
}